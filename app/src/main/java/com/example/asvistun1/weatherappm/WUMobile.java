package com.example.asvistun1.weatherappm;

import android.webkit.WebView;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;


/**
 * Created by asvistun1 on 11/30/2016.
 */

public class WUMobile {

        //Weather Underground token for asvistun1
        private final String TOKEN = "b57f96736eaeb97b";

        //Global JSON jse element
        JsonElement jse = null;
        JsonElement rjse = null;

    public void fetchJSE(String ZIP)
        { //Builds JSON object jse for use by other methods

            try
            {
                String url = "http://api.wunderground.com/api/" + TOKEN +
                        "/conditions/forecast10day/q/" + ZIP + ".json";

                //Forms URL
                URL wURL = new URL(url);

                //Creates Input Stream
                InputStream is = wURL.openStream();

                //Attaches to a BufferedReader
                BufferedReader br = new BufferedReader(new InputStreamReader(is));

                //Attaches JSON parser
                jse = new JsonParser().parse(br);

                is.close();
                br.close();


            }
            catch (java.io.UnsupportedEncodingException uee)
            {
                uee.printStackTrace();
            }
            catch (java.net.MalformedURLException mue)
            {
                mue.printStackTrace();
            }
            catch (java.io.IOException ioe)
            {
                ioe.printStackTrace();
            }
        }

        public boolean isEmpty(){
            //returns true if jse is empty; if location does not return info
            String error;
            error = jse.getAsJsonObject().get("response").getAsJsonObject()
                    .get("error").getAsJsonObject().get("type").getAsString();
            if(error.equals("querynotfound")) {
                return true;
            }
            else{
                return false;
            }

        }

        public String fetchCurCITY()
        {
            return jse.getAsJsonObject().get("current_observation").
                    getAsJsonObject().get("display_location").
                    getAsJsonObject().get("city").
                    getAsString();
        }

        public String fetchCurSTATE()
        { // modified form while debugging
            return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("display_location").getAsJsonObject().get("state").getAsString();
        }


        public String fetchCurTEMP()
        {
            return jse.getAsJsonObject().get("current_observation").
                    getAsJsonObject().get("temp_f").
                    getAsString();
        }

        public String fetchCurCOND()
        {
            return jse.getAsJsonObject().get("current_observation").
                    getAsJsonObject().get("weather").
                    getAsString();
        }


        public String fetchCurHUMI()
        {
            return jse.getAsJsonObject().get("current_observation").
                    getAsJsonObject().get("relative_humidity").
                    getAsString();
        }

        public String fetchCurWIND()
        {
            return jse.getAsJsonObject().get("current_observation").
                    getAsJsonObject().get("wind_dir").
                    getAsString();
        }

        public String fetchCurWINDS()
        {
            return jse.getAsJsonObject().get("current_observation").
                    getAsJsonObject().get("wind_mph").
                    getAsString();
        }

        public String fetchCurFEELS()
        {
            return jse.getAsJsonObject().get("current_observation").
                    getAsJsonObject().get("feelslike_f").
                    getAsString();
        }

        public String fetchICON() {
            return jse.getAsJsonObject().get("current_observation").
                    getAsJsonObject().get("icon").
                    getAsString();
        }

        public String fetchName10Day(int d)
        {
            return jse.getAsJsonObject().get("forecast").getAsJsonArray().get(d).getAsJsonObject().get("forecastday")
                    .getAsJsonObject().get("date").getAsJsonObject().get("weekday").getAsString();
        }

        public String fetchIcon10Day(int d)
        {
            return jse.getAsJsonObject().get("forecast").getAsJsonArray().get(d).getAsJsonObject().get("forecastday")
                    .getAsJsonObject().get("icon").getAsString();
        }

        public String fetchTempHigh10Day(int d)
        {
            return jse.getAsJsonObject().get("forecast").getAsJsonArray().get(d).getAsJsonObject().get("forecastday")
                    .getAsJsonObject().get("high").getAsJsonObject().get("fahrenheit").getAsString();
        }

        public String fetchTempLow10Day(int d)
        {
            return jse.getAsJsonObject().get("forecast").getAsJsonArray().get(d).getAsJsonObject().get("forecastday")
                    .getAsJsonObject().get("low").getAsJsonObject().get("fahrenheit").getAsString();

        }

    }
