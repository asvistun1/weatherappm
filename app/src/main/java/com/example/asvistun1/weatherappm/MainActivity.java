package com.example.asvistun1.weatherappm;

import android.content.Context;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void getWeather(View v)
    {
        // Disable threading. We'll fix this later.
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        // WIP
        EditText location = (EditText) findViewById(R.id.searchText);
        WUMobile w = new WUMobile();
        w.fetchJSE(location.getText().toString());

        if(w.isEmpty()){
            //Show an error text that the zip/city is invalid
            TextView error = (TextView) findViewById(R.id.error);
            error.setVisibility(View.VISIBLE);
        }
        else {

            // Set the text of the temperature field
            TextView temp = (TextView) findViewById(R.id.theTemp);
            temp.setText("" + w.fetchCurTEMP());

            TextView loc = (TextView) findViewById(R.id.theLocation);
            loc.setText("" + w.fetchCurCITY() + ", " + w.fetchCurSTATE());

            TextView condition = (TextView) findViewById(R.id.theCondition);
            condition.setText("" + w.fetchCurCOND());

            TextView feelsLike = (TextView) findViewById(R.id.theFeels);
            feelsLike.setText("Feels " + w.fetchCurFEELS());

            TextView curWind = (TextView) findViewById(R.id.theCurWind);
            curWind.setText("" + w.fetchCurWINDS() + " mph");

            TextView curHum = (TextView) findViewById(R.id.theHumidity);
            curHum.setText("Hum: " + w.fetchCurHUMI());

            ImageView img = (ImageView) findViewById(R.id.CurrentCondIcon);
            Context context = img.getContext();
            img.setImageResource(context.getResources().getIdentifier("drawable/" + w.fetchICON(), null, context.getPackageName()));

            String TOKEN = "b57f96736eaeb97b";
            EditText searchText = (EditText) findViewById(R.id.searchText);
            String ZIP = searchText.getText().toString();
            WebView radar = (WebView) findViewById(R.id.Radar);
            radar.loadUrl("http://api.wunderground.com/api/" + TOKEN + "/radar/satellite/q/" + ZIP + ".png?radius=100&width=400&height=400");

            RelativeLayout rl;
            rl = (RelativeLayout) findViewById(R.id.RelativeLayout1);

            switch (w.fetchICON()) {
                case "chanceflurries":
                case "chancesnow":
                case "flurries":
                case "snow":
                    rl.setBackgroundResource(R.drawable.snowy);
                    break;

                case "chancerain":
                case "rain":
                    rl.setBackgroundResource(R.drawable.rainy);
                    break;

                case "chancesleet":
                case "sleet":
                    rl.setBackgroundResource(R.drawable.snowy);
                    break;

                case "chancetstorms":
                case "tstorms":
                    rl.setBackgroundResource(R.drawable.lightning);
                    break;

                case "clear":
                case "sunny":
                    rl.setBackgroundResource(R.drawable.sun);
                    break;

                case "cloudy":
                    rl.setBackgroundResource(R.drawable.cloud);
                    break;

                case "fog":
                case "hazy":
                    rl.setBackgroundResource(R.drawable.foggy);
                    break;

                case "mostlycloudy":
                case "partlycloudy":
                    rl.setBackgroundResource(R.drawable.cloud);
                    break;

                case "mostlysunny":
                case "partlysunny":
                    rl.setBackgroundResource(R.drawable.partlysun);
                    break;

                default:
                    break;
            }
        }
    }
}
